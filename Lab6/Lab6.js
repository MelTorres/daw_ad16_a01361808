
var correcto=false;

function allowDrop(ev) {
    ev.preventDefault();
    correcto=true;
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}

function checar(){
    if(correcto===true){
        alert("Bien hecho!");
    }
    else{
        alert("Arrastra la imagen al cuadro");
    }
}


function ayuda() {
    var x;
    if (confirm("Arrastra la imagen al cuadro. ¿Te fue útil la información?") == true) {
    } else {
       alert("Lo sentimos!");
    }
}

var change = document.getElementById("cambia");
    change.onmouseover = function() {
    change.style.color = "black";
    change.innerHTML = "Lleva las manzanas a su lugar! Si tienes dudas, acerca el cursor a este texto o presiona el botón de ayuda.";
     }

var change2 = document.getElementById("cambia");
    change2.onmouseout = function() {
    change2.style.color = "gray";
    change2.innerHTML = "Arrastra la imagen al cuadro!";
     }