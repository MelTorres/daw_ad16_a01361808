<?php include("_header.html"); ?>

<?php

function conectDb(){
    
        $servername = "localhost";
        $username = "meltorres";
        $password = "";
        $dbname = "supermercado";
        
         $con = mysqli_connect ($servername, $username, $password, $dbname);
        
        if(!$con){
            die("Connection failed: " . mysqli_connect_error());
            echo ":O Hubo un error!";
        }
            
            
        return $con;
}
            
    function closeDb ($mysql){
        
        mysqli_close($mysql);
        
    }
    


 

    function getFruits(){
        
        $listaFrutas="<table><thead><strong>Lista de frutas</strong></thead>";
        $conn = conectDb();
        $sql = "SELECT id, name, units, quantity, price, country FROM fruit";
        $result = $conn->query ($sql);
        if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_assoc($result)){
                $listaFrutas .= "<tr>"." "."<td>" . $row["name"] . "</td>". " "."<td>$" . $row["price"] . "</td>". " "."<td>"."   " . $row["country"] . "</td>". " ". "</tr>";
            }
            
        }
        $listaFrutas.="</table><br>";
        
        closeDb($conn);
        return $listaFrutas;
    }
    
    
    function sortByCountry(){
        $listaFrutas="<table><thead><strong>Lista de frutas ordenada por pais</strong></thead>";
        $conn = conectDb();
        $sql = "SELECT * FROM  `fruit` ORDER BY  `country` ";
        $result = $conn->query ($sql);
         if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_assoc($result)){
                $listaFrutas .= "<tr>"." "."<td>" . $row["name"] . "</td>". " "."<td>$" . $row["price"] . "</td>". " "."<td>"."   " . $row["country"] . "</td>". " ". "</tr>";
            }
            
        }
        $listaFrutas.="</table><br>";
        
        closeDb($conn);
        return $listaFrutas;
    }
    
     function sortByPrice(){
        $listaFrutas="<table><thead><strong>Lista de frutas ordenada por precio</strong></thead>";
        $conn = conectDb();
        $sql = "SELECT * FROM  `fruit` ORDER BY  `price` ";
        $result = $conn->query ($sql);
         if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_assoc($result)){
                $listaFrutas .= "<tr>"." "."<td>" . $row["name"] . "</td>". " "."<td>$" . $row["price"] . "</td>". " "."<td>"."   " . $row["country"] . "</td>". " ". "</tr>";
            }
            
        }
        $listaFrutas.="</table><br>";
        
        closeDb($conn);
        return $listaFrutas;
    }
    
    
?>
    

    <?php include("_footer.html"); ?>
