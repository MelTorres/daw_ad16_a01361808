<?php
$target_dir = "imagenes/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;//paso filtros
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
$actual=" ";
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if($check !== false) {
                $actual= $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                $actual= "Formato incorrecto.";
                $uploadOk = 0;
            }
        
        // Check if file already exists
        if (file_exists($target_file)) {
            $actual= "Ya existe el archivo.";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            $actual= "El archivo excede dimensiones.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $actual= "Formato incorrecto.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $actual= " ";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                $actual= "El archivo ". basename( $_FILES["fileToUpload"]["name"]). " se subió con éxito!";
            } else {
                $actual= "Error al subir";
            }
        }
        
            $file = $_FILES['fileToUpload']['name'];
                        include("index.html");

    }
    else{
        header("location: login.php");

    }
?>