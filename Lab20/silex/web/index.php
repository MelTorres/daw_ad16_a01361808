<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

$app->get('/hello', function () {
    return "Hello, it's me. I was wondering if after all these 
    years. You'd like to meet, to go over everything.";
});



$app->get('/hello/{name}', function ($name) use ($app) {
    return 'Hello '.$app->escape($name);
});

$blogPosts = array(
    1 => array(
        'date'      => '2011-03-29',
        'author'    => 'igorw',
        'title'     => 'Pregunta 1',
        'body'      => '¿A qué se refiere la descentralización de servicios web? A la fragmentación del servicio de tal modo que si uno de los servidores falla, no deje de funcionar la aplicación, sino que utilice otro.',
        'id'      => '1',
    ),
     2 => array(
        'date'      => '2011-03-29',
        'author'    => 'igorw',
        'title'     => 'Pregunta 2',
        'body'      => '¿Cómo puede implementarse un entorno con servicios web disponibles aún cuando falle un servidor? Cuando se monta el servicio web se hace sobre varios servicios locales, de tal manera que el servicio esté descentralizado y no se dependa unicamnete de un servidor.',
        'id'      => '2',
    ), 
    3 => array(
        'date'      => '2011-03-29',
        'author'    => 'igorw',
        'title'     => 'Segunda parte',
        'body'      => 'ingresa a https://daw-bd-meltorres.c9users.io/Lab20/silex/web/index.php/hello y a https://daw-bd-meltorres.c9users.io/Lab20/silex/web/index.php/hello/bernie/tuNombre para ver la segunda parte',
        'id'      => '3',
    ),
);

$app->get('/blog', function () use ($blogPosts) {
    $output = '';
    foreach ($blogPosts as $post) {
        $output .= "<a class='letra' href=https://daw-bd-meltorres.c9users.io/Lab20/silex/web/index.php/blog/{$post['id']}><p>{$post['title']}</p></a>";
        $output .= '<br />';
    }

    return $output;
});

$app->get('/blog/{id}', function (Silex\Application $app, $id) use ($blogPosts) {
    if (!isset($blogPosts[$id])) {
        $app->abort(404, "Post $id does not exist.");
    }

    $post = $blogPosts[$id];

    return  "<h1>{$post['title']}</h1>".
            "<h6>{$post['author']}</h6>".
            "<p>{$post['body']}</p>";
});

$app->get('/index.php', function () use ($app) {
    return $app->redirect('/Lab20/silex/web/index.php/blog');
});


$app->run();

?>
